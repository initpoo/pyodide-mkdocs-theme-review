---
author: Mireille Coilhac
title: Images  en Python
---



## I. Utiliser la bibliothèque matplotlib par Nicolas Revéret


???+ question "La courbe 1 s'affiche dans une admonition sous l'éditeur"


    {{ IDE('exo_figure_1')}}
    
    ??? tip "Votre figure"
        <div id="cible_1" class="center" style="display: flex;justify-content: center;align-content:center;flex-direction: column;margin:auto;min-height:5em;text-align:center">
        Votre tracé sera ici
        </div>



???+ question "La courbe 2 s'affiche dans une admonition sous l'éditeur"

    {{ IDE('exo_figure_2')}}

    ??? tip "Votre figure"
        <div id="cible_2" class="center" style="display: flex;justify-content: center;align-content:center;flex-direction: column;margin:auto;min-height:5em;text-align:center">
        Votre tracé sera ici
        </div>


## II. Utiliser la tortue par Romain Janvier


???+ question "Utilisation de la tortue"

    {{ IDE('arbre_tortue') }}
    
    <div id="cible_3" class="admonition center" style="display: flex;justify-content: center;align-content:center;flex-direction: column;margin:auto;min-height:5em;text-align:center">
    Le  tracé sera affiché ici
    </div>



